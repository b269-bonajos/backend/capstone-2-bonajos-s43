const Order = require('../models/Order');

// Create a new order
module.exports.createOrder = async (req, res) => {
  const { userId, items, shippingAddress } = req.body;

  // Validate input parameters
  if (!userId || !items || !shippingAddress) {
    return res.status(400).json({ message: 'Incomplete order information' });
  }

  try {
    // Create a new order in the database
    const order = await Order.create({
      userId,
      items,
      shippingAddress,
      status: 'pending'
    });

    return res.status(201).json({ orderId: order.id });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Error creating order' });
  }
};

// Get information about a specific order
module.exports.getOrder = async (req, res) => {
  const { id } = req.params;

  try {
    // Find the order in the database
    const order = await Order.findOne({ where: { id } });

    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }

    return res.status(200).json({ order });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Error retrieving order' });
  }
};

// Update an existing order
module.exports.updateOrder = async (req, res) => {
  const { id } = req.params;
  const { status } = req.body;

  try {
    // Find the order in the database
    const order = await Order.findOne({ where: { id } });

    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }

    // Update the order status
    order.status = status || order.status;
    await order.save();

    return res.status(200).json({ message: 'Order updated successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Error updating order' });
  }
};

// Get a list of all orders
module.exports.getAllOrders = async (req, res) => {
  try {
    // Find all orders in the database
    const orders = await Order.findAll();

    return res.status(200).json({ orders });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Error retrieving orders' });
  }
};

const product = require("../models/Product");


module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		let newproduct = new product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});
		return newproduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};



// Retrieve All Active products
module.exports.getAllActive = () => {
	return product.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getProduct = (reqParams) => {
	return product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
	};

	return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};




module.exports.archiveProduct = (reqParams, ) => {
	let achivingAProduct = {
		isActive: false
	};

	return product.findById(reqParams.courseId, achivingAProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


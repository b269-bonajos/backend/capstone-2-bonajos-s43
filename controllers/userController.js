const User = require("../models/User");

// bcrypt is a passwod-hasing function that is commonly used in computer systems to store user pasword securely
const bcrypt = require("bcrypt");

const auth = require("../auth");
const Product = require("../models/Product");


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		// User does not exist
		if (result == null) {
			return false
			// User exists
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

// // S38 Activity

// /*
// Business Logic:
// 1. Find the document in the database using the user's ID
// 2. Reassign the password of the returned document to an empty string
// 3. Return the result back to the frontend
// */

// module.exports.getProfile = (data) => {
// 	return User.findById(data.userId).then(result => {
// 		result.password = "";
// 		return result;
// 	});
// };

// // Authenticated user enrollment

// /*
// Business Logic:
// 1. Find the document in the database using the user's ID
// 2. Add the course ID to the user's enrollment array
// 3. Update the document in the MongoDB Atlas Database
// */

// module.exports.enroll = async (data) => {
// 	// Add course ID in the enrollments array of the user
// 	let isUserUpdated = await User.findById(data.userId).then(user => {
// 		user.enrollments.push({courseId: data.courseId});
// 		return user.save().then((user, error) =>{
// 			if(error){
// 				return false;
// 			} else {
// 				return true;
// 			};
// 		});
// 	});
// 	// Add user ID in the enrollees array of the course
// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
// 		course.enrollees.push({userId: data.userId});
// 		return course.save().then((course, error) => {
// 			if (error){
// 				return false;
// 			} else {
// 				return true;
// 			};
// 		});
// 	});

// 	if(isUserUpdated && isCourseUpdated){
// 		return true;
// 	} else {
// 		return false;
// 	};
// };



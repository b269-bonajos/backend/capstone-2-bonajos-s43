const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");



// Route for user registration
router.post("/register", (req, res) => { 
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// // Route for retrieving user details
// router.post("/details", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
// })

// // Route for authenticated user enrollment
// router.post("/enroll", auth.verify, (req, res) =>{
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		courseId: req.body.courseId
// 	}
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// })

// // Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;

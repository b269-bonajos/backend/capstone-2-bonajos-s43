const express = require('express');
const router = express.Router();
const order = require('../models/Order');
const orderController = require("../controllers/orderController");
router.post('/orders/create', async (req, res) => {
  const { userId, items, shippingAddress } = req.body;

  // Validate input parameters
  if (!userId || !items || !shippingAddress) {
    return res.status(400).json({ message: 'Incomplete order information' });
  }

  try {
    // Create a new order in the database
    const order = await Order.create({
      userId,
      items,
      shippingAddress,
      status: 'pending'
    });

    return res.status(201).json({ orderId: order.id });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Error creating order' });
  }
});

module.exports = router;
